from . import bitbucket_cloud_algo

def test_bitbucket_cloud_algo():
    assert bitbucket_cloud_algo.apply("Jane") == "hello Jane"
